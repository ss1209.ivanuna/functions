const getSum = (str1, str2) => {
  if (typeof (str1) !== 'string' || typeof (str2) !== 'string') {
    return false;
  }

  const result = Number(str1) + Number(str2);

  if (Number.isNaN(result)) return false;

  return String(result);
}

function getQuantityPostsByAuthor(listOfPosts, authorName) {
  let PostQuantity = 0;
  let commentsQuantity = 0;

  listOfPosts.forEach((Post) => {
    if (Post.author === authorName) PostQuantity += 1;

    Post.comments?.forEach((comment) => {
      if (comment.author === authorName) commentsQuantity += 1;
    });
  });

  return `Post:${PostQuantity},comments:${commentsQuantity}`;
}

const tickets = (people) => {
  const cost = 25;
  let clerkValue = 0;
  let result = 'YES';

  for(const customerValue of people) {   
    if (customerValue > cost && clerkValue < customerValue - cost) {
      result = 'NO';
      break;
    }
    clerkValue += cost;
  }

  return result;
}

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
